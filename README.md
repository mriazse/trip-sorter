# Trip Sorter
Trip sorter algorithm takes un-sorted boarding passes and sort them in proper order based on source and destinations provided in the boarding passes.

##Get the project repository
Clone the repository into your projects directory, i.e : ~/projects/trip-sorter

```git
git clone git@bitbucket.org:mriazse/trip-sorter.git ~/projects/trip-sorter
```

##Installation
To install the Trip sorter just run [composer](http://getcomposer.org) install, please make sure [composer](http://getcomposer.org) is installed and it is up to date.

```bash
php composer.phar install
```
##Running Tests
To run the test use following command in your project root

```bash
vendor/bin/phpunit tests
```