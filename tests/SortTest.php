<?php
namespace Tripsorter\Test;

use \PHPUnit_Framework_TestCase;

use Tripsorter\lib\entity\BoardingCard\BoardingCardInterface;
use Tripsorter\lib\entity\BoardingCard\BusBoardingCard;
use Tripsorter\lib\entity\BoardingCard\FlightBoardCard;
use Tripsorter\lib\entity\Location;
use Tripsorter\Tripsorter;

class SortTest extends PHPUnit_Framework_TestCase
{
    public function testTripSort()
    {
        $location1 = new Location('marina', 'mr');
        $location2 = new Location('barsha', 'br');
        $location3 = new Location('deira', 'dr');
        $location4 = new Location('sharja', 'shr');

        $busBoardingCard1 = new BusBoardingCard($location1, $location2);
        $busBoardingCard2 = new BusBoardingCard($location2, $location3);
        $busBoardingCard3 = new BusBoardingCard($location3, $location4);

        $sorted = Tripsorter::getInstance()->sort(
            [
                $busBoardingCard3,
                $busBoardingCard1,
                $busBoardingCard2
            ]
        );

        $this->assertCount(3, $sorted);
        $this->assertEquals($busBoardingCard1, $sorted[0]);
        $this->assertEquals($busBoardingCard2, $sorted[1]);
        $this->assertEquals($busBoardingCard3, $sorted[2]);
    }
} 