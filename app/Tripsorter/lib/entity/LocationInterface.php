<?php
namespace Tripsorter\lib\entity;

/**
 * Interface LocationInterface
 * @package Tripsorter\lib\entity
 */
interface LocationInterface
{
    /**
     * Returns name of the location
     *
     * @return string
     */
    public function getName();

    /**
     * Returns code of the location
     *
     * @return string
     */
    public function getCode();
} 