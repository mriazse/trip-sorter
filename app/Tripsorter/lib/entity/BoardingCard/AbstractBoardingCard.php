<?php
namespace Tripsorter\lib\entity\BoardingCard;

use Tripsorter\lib\entity\LocationInterface;

/**
 * Class AbstractBoardingCard
 * @package Tripsorter\lib\entity\BoardingCard
 */
abstract class AbstractBoardingCard implements BoardingCardInterface
{
    /**
     * @var LocationInterface
     */
    protected $source;

    /**
     * @var LocationInterface
     */
    protected $destination;

    function __construct(LocationInterface $source, LocationInterface $destination)
    {
        $this->source      = $source;
        $this->destination = $destination;
    }

    /**
     * @inheritdoc
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @inheritdoc
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @inheritdoc
     */
    public function format()
    {
        return sprintf(
            'From %s to %s, Traportation Type: %s',
            $this->getSource()->getName(),
            $this->getDestination()->getName(),
            $this->getTransportationType()
        );
    }

    /**
     * @inheritdoc
     */
    public function getTransportationType()
    {
        return null;
    }
}