<?php
namespace Tripsorter\lib\entity\BoardingCard;

class FlightBoardCard extends AbstractBoardingCard
{
    /**
     * @inheritdoc
     */
    public function getTransportationType()
    {
        return 'Flight';
    }
} 