<?php
namespace Tripsorter\lib\entity\BoardingCard;

use Tripsorter\lib\entity\LocationInterface;

/**
 * Interface BoardingCardInterface
 * @package Tripsorter\lib\entity\BoardingCard
 */
interface BoardingCardInterface
{
    /**
     * Returns source of boarding card
     *
     * @return LocationInterface
     */
    public function getSource();

    /**
     * Returns destination of boarding card
     *
     * @return LocationInterface
     */
    public function getDestination();

    /**
     * Returns type of the boarding card
     * @return string
     */
    public function getTransportationType();

    /**
     * Format source and destination
     *
     * @return string
     */
    public function format();
}