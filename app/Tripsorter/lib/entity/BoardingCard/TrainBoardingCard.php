<?php
namespace Tripsorter\lib\entity\BoardingCard;

/**
 * Class TrainBoardingCard
 * @package Tripsorter\lib\entity\BoardingCard
 */
class TrainBoardingCard extends AbstractBoardingCard
{
    /**
     * @inheritdoc
     */
    public function getTransportationType()
    {
        return 'Train';
    }
} 