<?php
namespace Tripsorter\lib\entity\BoardingCard;

use Tripsorter\lib\entity\LocationInterface;

/**
 * Class BusBoardingCard
 * @package Tripsorter\lib\entity\BoardingCard
 */
class BusBoardingCard extends AbstractBoardingCard
{
    public function getTransportationType()
    {
        return 'Bus';
    }
} 