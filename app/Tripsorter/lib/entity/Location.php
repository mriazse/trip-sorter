<?php
namespace Tripsorter\lib\entity;

use Tripsorter\lib\entity\LocationInterface;

/**
 * Class Location
 * @package Tripsorter\lib\entity
 */
class Location implements LocationInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $code;

    function __construct($name, $code)
    {
        $this->name = $name;
        $this->code = $code;
    }

    /**
     * @inheritdoc
     */
    function getName()
    {
       return $this->name;
    }

    /**
     * @inheritdoc
     */
    function getCode()
    {
        return $this->code;
    }
} 