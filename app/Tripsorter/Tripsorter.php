<?php
namespace Tripsorter;

class Tripsorter
{
    protected static $instance = null;

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Sorts array
     *
     * @param array $boardingCards
     * @return array
     */
    public function sort(array $boardingCards)
    {
        $sortedResult = [array_pop($boardingCards)];

        for ($i = 0; $i < count($boardingCards); $i++) {

            /**
             * @var BoardingCardInterface $card
             */
            foreach ($boardingCards as $key => $card) {
                if (end($sortedResult)->getDestination()->getCode() === $card->getSource()->getCode()) {
                    array_push($sortedResult, $card);
                    unset($boardingCards[$key]);
                } elseif (reset($sortedResult)->getSource()->getCode() === $card->getDestination()->getCode()) {
                    array_unshift($sortedResult, $card);
                    unset($boardingCards[$key]);
                }
            }
        }

        return $sortedResult;
    }
} 